function makeServer() {
    var express = require('express')
    var app = express()

    // The code below will display 'Hello World!' to the browser when you go to http://localhost:3000 
    app.get('/', function (req, res) {
        res.status(200).send('Hello World!')
    })

    var server = app.listen(3000, function () {
        var port = server.address().port;
        console.log('Example app listening on port %s!', port)
    })
    return server;
}

module.exports = makeServer;